## Progress Log

You can check the progress log here [LOG.md](./LOG.md)

## Description

Technical Test Application for torre.co., this project was development with [NEST JS](https://nestjs. com) for the backend and [Angular](https://angular.io) for the frontend. Here I tried to create a way to measure skill competency based on creating a certification program, each user will be complete a test based on a specific skill, each skill can have many levels (pe. Rookie, Medium, Advance).

1. Search engine by skill https://torre.inger.co/main, 
2. Search result (pe. Angular) https://torre.inger.co/main/angular
3. Profile view pe. https://torre.inger.co/profile/leonardorive6
4. Test page pe. https://torre.inger.co/profile/leonardorive6/certification/angular / https://torre.inger.co/profile/borgepab3456/certification/react

## Aspects to Improve

1. Write more UnitTest (JEST).
2. Write more Integrarion test (PostMan).
3. Add capability to save the test result.
4. Manage certification levels.
5. Add Unit Test to the FrontEnd

## Installation
##### BACKEND https://gitlab.com/torre-tech-test/torre-api
```bash
$ npm install
```
##### FRONTEND https://gitlab.com/torre-tech-test/torre-front
```bash
$ npm install
```

## Running the app
##### BACKEND
```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

##### FRONTEND
```bash
# development
$ npm run start
```

## Test
##### BACKEND
```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
