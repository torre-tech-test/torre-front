## Progress Log

### Mar 15 2021
* **11:00** Receipt of the technical test email.
* **12:00** Analysis of the problem.
* **14:00** Exploring torre.co, checking how the different Endpoints received work.
* **16:00** Exploring CSS code from torre.co, checking what can I use on my test (I decided to reuse CSS code for the hexagon logo).
### Mar 16 2021
* **08:00** Bootstrap the BackEnd project.
* **09:00** Start creating the Entities for the project.
* **10:00** Start creating the test for the service, **JEST** configuration.
* **11:00** Start creating the Services for the project.
### Mar 17 2021
* **08:00** Bootstrap the FrontEnd project.
* **09:00** Start creating the Components for the project.
* **10:00** Creation of search component.
* **11:00** Fix styles search component.
* **14:00** Creation of result component.
* **15:00** Fix layout and styles for result component.
* **16:00** Creation of profile component.
* **17:00** Fix layout and styles for profile component.
### Mar 18 2021
* **08:00** Creation of test component.
* **09:00** Fix layout and styles for result component.
* **10:00** Implementation user functionality.
* **11:00** Project deployed and sended.
