import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'main',
        pathMatch: 'full'
      },
      {
        path: 'main',
        loadChildren: () => import('../app/search-profile/search-profile.module').then(m => m.SearchProfileModule),
      },
      {
        path: 'profile',
        loadChildren: () => import('../app/profile/profile.module').then(m => m.ProfileModule),
      },
      {
        path: 'profile',
        loadChildren: () => import('../app/certification/certification.module').then(m => m.CertificationModule),
      },
    ],
  },
  // otherwise redirect to main
  { path: '**', redirectTo: 'main' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollOffset: [0, 0], scrollPositionRestoration: 'top', relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
