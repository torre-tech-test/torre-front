import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-skill-proficiency',
  templateUrl: './profile-skill-proficiency.component.html',
  styleUrls: ['./profile-skill-proficiency.component.scss']
})
export class ProfileSkillProficiencyComponent implements OnInit {
  @Input()
  profile: any;

  constructor() { }

  ngOnInit(): void {
  }

}
