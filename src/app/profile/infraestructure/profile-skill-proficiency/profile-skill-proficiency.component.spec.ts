import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileSkillProficiencyComponent } from './profile-skill-proficiency.component';

describe('ProfileSkillProficiencyComponent', () => {
  let component: ProfileSkillProficiencyComponent;
  let fixture: ComponentFixture<ProfileSkillProficiencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileSkillProficiencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileSkillProficiencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
