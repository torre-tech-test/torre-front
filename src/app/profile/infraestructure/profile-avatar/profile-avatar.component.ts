import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-avatar',
  templateUrl: './profile-avatar.component.html',
  styleUrls: ['./profile-avatar.component.scss']
})
export class ProfileAvatarComponent implements OnInit {
  _profile: any;
  avatar: string;
  letter: string = 'T';
  useAvatar = false;

  @Input()
  set profile(value: any) {
    this._profile = value;
    this.useAvatar = !!value.picture;

    this.avatar = value.picture;
    this.letter = (value.name || ' ')[0].toUpperCase();
  }

  constructor() { }

  ngOnInit(): void {
  }

}
