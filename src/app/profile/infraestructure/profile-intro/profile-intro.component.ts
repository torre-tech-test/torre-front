import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-profile-intro',
  templateUrl: './profile-intro.component.html',
  styleUrls: ['./profile-intro.component.scss']
})
export class ProfileIntroComponent {
  languages: any[];
  interests: any[];
  industries: { data: any[] };

  _profile: any;
  @Input()
  set profile(value: any) {
    if (!value) {
      return;
    }

    this._profile = value;

    this.languages = value.languages || [];
    this.interests = value.interests || [];
    this.industries = value.opportunities.find(
      op => op.interest === 'industries'
    ) || { data: []};
  }

  get profile(): any {
    return this._profile;
  }
}
