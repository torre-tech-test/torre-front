import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { SearchUserTorreService } from '../../domain/search-user-torre.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  subs = new SubSink();
  profile: any;
  contentLoaded = false;
  loadingMessage = '<h1 class="text-white-50 text-center">Loading...</h1>';

  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    protected searchUserTorreSrv: SearchUserTorreService,
  ) { }

  ngOnInit(): void {
    this.subs.sink = this.route.params.pipe(
      mergeMap(
        params => {
          if (!params.username) {
            this.router.navigateByUrl('/main');
            return null;
          }

          return this.searchUserTorreSrv.searchUser(params.username);
        }
      ),
    ).subscribe(
      data => {
        this.contentLoaded = !!data;
        this.profile = data;
      },
      error => {
        console.log(error);
        this.loadingMessage = `<h1 class="text-danger text-center"><i class="mdi mdi-comment-alert"></i> ${ error.error.message }</h1>`;
      }
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
