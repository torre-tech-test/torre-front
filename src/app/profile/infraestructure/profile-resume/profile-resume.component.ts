import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-resume',
  templateUrl: './profile-resume.component.html',
  styleUrls: ['./profile-resume.component.scss']
})
export class ProfileResumeComponent implements OnInit {
  @Input()
  profile: any;

  constructor() { }

  ngOnInit(): void {
  }

}
