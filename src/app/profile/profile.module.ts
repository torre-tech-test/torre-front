import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './infraestructure/profile/profile.component';
import { ProfileAvatarComponent } from './infraestructure/profile-avatar/profile-avatar.component';
import { ProfileCardComponent } from './infraestructure/profile-card/profile-card.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileIntroComponent } from './infraestructure/profile-intro/profile-intro.component';
import { ProfileSkillProficiencyComponent } from './infraestructure/profile-skill-proficiency/profile-skill-proficiency.component';
import { ProfileResumeComponent } from './infraestructure/profile-resume/profile-resume.component';

@NgModule({
  declarations: [
    ProfileAvatarComponent,
    ProfileCardComponent,
    ProfileComponent,
    ProfileIntroComponent,
    ProfileSkillProficiencyComponent,
    ProfileResumeComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ProfileRoutingModule,
  ],
  exports: [
    ProfileAvatarComponent,
    ProfileCardComponent,
    ProfileComponent,
  ]
})
export class ProfileModule { }
