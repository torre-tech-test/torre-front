import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SubSink } from 'subsink';
import { SearchTorreService } from '../../domain/search-torre.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit, OnDestroy {
  private subs = new SubSink();

  aggregators: any;
  offset = 0;
  results: any[];
  size = 10;
  total: number;

  constructor(
    protected searchTorreSrv: SearchTorreService,
    protected route: ActivatedRoute,
    protected router: Router,
  ) {
    this.results = Array(this.size).fill({loading: true});
  }

  ngOnInit(): void {
    this.subs.sink = this.route.params.subscribe(
      params => {
        if (!params.criteria) {
          this.router.navigateByUrl('/main');
        }

        const criteria = params.criteria;

        this.searchTorreSrv.searchPeople(
          { offset: this.offset, size: this.size },
          {
            'skill/role': {
              text: criteria,
              experience: '1-plus-year'
            }
          }
        ).subscribe(
          ({ aggregators, offset, results, size, total}) => {
            this.offset = offset;
            this.results = results;
            this.size = size;
            this.total = total;
          }
        );
      }
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
