import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  frmSearch: FormGroup;

  constructor(
    fb: FormBuilder,
    protected router: Router,
  ) {
    this.frmSearch = fb.group({
      criteria: [null, [Validators.required, Validators.minLength(3)]]
    });
  }

  ngOnInit(): void {
  }

  search() {
    console.log(this.frmSearch);

    if (this.frmSearch.invalid) {
      return;
    }

    this.router.navigateByUrl(`/main/${ this.frmSearch.value.criteria }`);
  }
}
