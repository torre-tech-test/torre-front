import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SearchProfileRoutingModule } from './search-profile-routing.module';

import { SearchComponent } from './infraestructure/search/search.component';
import { ResultComponent } from './infraestructure/result/result.component';
import { PaginatorComponent } from './infraestructure/paginator/paginator.component';
import { ProfileModule } from '../profile/profile.module';

@NgModule({
  declarations: [
    PaginatorComponent,
    ResultComponent,
    SearchComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SearchProfileRoutingModule,

    ProfileModule,
  ]
})
export class SearchProfileModule { }
