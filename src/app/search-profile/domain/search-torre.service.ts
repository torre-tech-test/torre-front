import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class SearchTorreService {
  constructor(protected http: HttpClient) {}

  searchPeople(criteria: { offset?: number, size?: number, aggregate?: boolean }, model: any): Observable<any> {
    const defaultParams = { offset: 0, size: 10, aggregate: true };
    const params = { ...defaultParams, ...criteria };
    const query = Object.keys(params).map(key => `${key}=${params[key]}`);

    return this.http.post(`https://search.torre.co/people/_search/?${ query.join('&') }`, model);
  }
}
