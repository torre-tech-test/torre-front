import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './infraestructure/search/search.component';
import { ResultComponent } from './infraestructure/result/result.component';

const routes: Routes = [
  { path: '', component: SearchComponent },
  { path: ':criteria', component: ResultComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchProfileRoutingModule {}
