import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertificationRoutingModule } from './certification-routing.module';
import { ViewCertificationComponent } from './infraestructure/view-certification/view-certification.component';
import { TakeCertificationComponent } from './infraestructure/take-certification/take-certification.component';

@NgModule({
  declarations: [ViewCertificationComponent, TakeCertificationComponent],
  imports: [
    CommonModule,
    CertificationRoutingModule,
  ]
})
export class CertificationModule { }
