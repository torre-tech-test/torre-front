import { Question } from "./question.entity";

export interface Certification {
  certId: number;
  certSkill: string;
  certImage: string;
  certName: string;
  certMinRequired: number;
  certOrder: number;
  certQuestionList: Question[];
}
