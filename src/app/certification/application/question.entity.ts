import { Certification } from "./certification.entity";

export enum TypeQuestion {
  CHECKBOX = 'CHECKBOX',
  RADIO = 'RADIO',
  MATRIX = 'MATRIX',
  DATE = 'DATE',
  TEXT = 'TEXT',
  MEMO = 'MEMO',
}

export interface OptionQuestion {
  language: string;
  response: number;
  // Nulleable properties
  options: string[];
}

export interface Question {
  queId: number;
  queLabel: string;
  queOptions: OptionQuestion;
  queTypeQuestion: TypeQuestion;
  queCertification: Certification;
  class?: string;
}
