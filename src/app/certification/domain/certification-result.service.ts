import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../../environments/environment';

@Injectable({providedIn: 'root'})
export class CertificationResultService {
  constructor(protected http: HttpClient) {}

  Find(username: string, skillName: string): Observable<any> {
    return this.http.get(`${environment.API.Url}/profile/${username}/certification/${skillName}`);
  }
}
