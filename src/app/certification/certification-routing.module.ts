import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewCertificationComponent } from './infraestructure/view-certification/view-certification.component';
import { TakeCertificationComponent } from './infraestructure/take-certification/take-certification.component';

const routes: Routes = [
  { path: ':publicId/certification/:skill/view', component: ViewCertificationComponent },
  { path: ':publicId/certification/:skill', component: TakeCertificationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertificationRoutingModule { }
