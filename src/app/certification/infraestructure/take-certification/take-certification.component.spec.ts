import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TakeCertificationComponent } from './take-certification.component';

describe('TakeCertificationComponent', () => {
  let component: TakeCertificationComponent;
  let fixture: ComponentFixture<TakeCertificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TakeCertificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TakeCertificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
