import { Component, OnInit, OnDestroy } from '@angular/core';
import { CertificationFindService } from '../../domain/certification-find.service';
import { ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';
import { mergeMap } from 'rxjs/operators';
import { Question } from '../../application/question.entity';
import { Certification } from '../../application/certification.entity';

@Component({
  selector: 'app-take-certification',
  templateUrl: './take-certification.component.html',
  styleUrls: ['./take-certification.component.scss']
})
export class TakeCertificationComponent implements OnInit, OnDestroy {
  subs = new SubSink();

  certification: Certification;
  showResponse = false;
  validInputs = [];
  indexSelected = -1;
  loaded = false;
  testFinished = false;
  question: Question;
  questionList: Question[];

  score = 0;

  constructor(
    protected route: ActivatedRoute,
    protected certificationFindSrv: CertificationFindService,
  ) { }

  ngOnInit(): void {
    this.subs.sink = this.route.params.pipe(
      mergeMap(
        ({publicId, skill}) => this.certificationFindSrv.Find(publicId, skill)
      )
    ).subscribe(
      result => {
        this.certification = result;
        this.questionList = JSON.parse(JSON.stringify(this.certification.certQuestionList));
        this.question = this.questionList.shift();

        setTimeout(() => this.loaded = true, 1200);

        this.generateValidInputs();
      }
    );
    document.addEventListener('keyup', ev => this.onKey(ev));
  }

  generateValidInputs() {
    this.validInputs = Array(this.question.queOptions.options.length).fill('0').map((item, index) => `${index + 1}`);

    console.log(this.question, this.validInputs);
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  response(index) {
    this.showResponse = true;
    this.indexSelected = index;

    this.score += this.indexSelected === this.question.queOptions.response ? 1 : 0;

    setTimeout(() => {
      this.showResponse = false;
      this.indexSelected = -1;

      if (this.questionList.length > 0) {
        this.question = this.questionList.shift();
        return this.generateValidInputs();
      }

      this.testFinished = true;
    }, 1500);
  }

  onKey($event) {
    if (this.validInputs.indexOf($event.key) >= 0) {
      const index = Number($event.key) - 1;

      this.response(index);
    }

    console.log($event);2
  }
}
