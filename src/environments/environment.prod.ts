const API_ROOT = 'https://api-torre.inger.co';

export const environment = {
  production: true,
  API: {
    Root: API_ROOT,
    Url: `${API_ROOT}/api/v1`,
  },
};
